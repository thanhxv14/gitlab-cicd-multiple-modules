package com.thanhxv.controller;

import com.thanhxv.response.TodoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/v1/todos")
public class TodoController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/{todoId}")
    public ResponseEntity<TodoResponse> pingTodo(@PathVariable("todoId") Integer todoId) {
        ResponseEntity<TodoResponse> response = restTemplate.getForEntity("https://dummyjson.com/todos/" + todoId, TodoResponse.class);
        return response;
    }

}
