package com.thanhxv.controller;

import com.thanhxv.response.IpifyResponse;
import com.thanhxv.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/v1/ping")
@RequiredArgsConstructor
public class PingController {
    @Value("${applications.profile}")
    private String profile;

    private final RestTemplate restTemplate;

    @GetMapping()
    public String ping() {
        return "ping........";
    }

    @GetMapping("/common")
    public String pingToCommon() {
        return "ping " + StringUtils.pingToCommon();
    }

    @GetMapping("/profile")
    public String profile() {
        return profile;
    }

}
