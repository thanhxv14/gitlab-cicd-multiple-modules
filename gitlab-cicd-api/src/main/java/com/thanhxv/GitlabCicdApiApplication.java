package com.thanhxv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCicdApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabCicdApiApplication.class, args);
	}

}
