package com.thanhxv.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IpifyResponse {

    @JsonProperty("ip")
    private String ip;

}
