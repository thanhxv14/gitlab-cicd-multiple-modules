package com.thanhxv.response;

import lombok.Data;

@Data
public class TodoResponse {
    private Integer id;
    private String todo;
    private boolean completed;
    private Integer userId;

}
