package com.thanhxv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCicdAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabCicdAuthApplication.class, args);
	}

}
