package com.thanhxv.controller;

import com.thanhxv.utils.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/ping")
public class PingController {
    @Value("${applications.profile}")
    private String profile;

    @GetMapping()
    public String auth() {
        return StringUtils.auth();
    }

    @GetMapping("/profile")
    public String profile() {
        return profile;
    }

}
